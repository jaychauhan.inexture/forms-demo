import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.css']
})
export class ProfileEditorComponent implements OnInit {
  profileForm = this.fb.group({
    firstName: ['', [
      Validators.required,
      Validators.minLength(4)
    ]],
    lastName: [''],
    address: this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    }),
    aliases: this.fb.array([
      this.fb.control('')
    ])
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.warn(this.profileForm.value);
  }

  updateProfile() {
    this.profileForm.patchValue({
      firstName: 'jayvitech',
      address: {
        street: '123 Drew Street',
      }
    });
  }

  get aliases() {
    return this.profileForm.get('aliases') as FormArray;
  }

  // convenience getter for easy access to form fields
  get f() { return this.profileForm.controls; }

  addAliases() {
    this.aliases.push(this.fb.control(''));
  }

}
