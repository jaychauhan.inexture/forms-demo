import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NameEditorComponent } from './reactive/name-editor/name-editor.component';
import { HeroFormComponent } from './template-driven/hero-form/hero-form.component';

const routes: Routes = [
  { path: 'reactive', component: NameEditorComponent },
  { path: 'template-driven', component: HeroFormComponent },
  { path: '', redirectTo: 'template-driven', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
